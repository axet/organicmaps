package com.mapswithme.maps.location;

import android.os.Build;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.github.axet.androidlibrary.activities.AppCompatThemeActivity;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.mapswithme.maps.MwmActivity;
import com.mapswithme.maps.R;
import com.mapswithme.maps.maplayer.MapButtonsController;
import com.mapswithme.maps.settings.BookmarksStoragePreferenceCompat;

import java.util.Date;

public class RecordingHelper {
    public static final String PAUSE_RECORDING = MwmActivity.class.getCanonicalName() + ".PAUSE_RECORDING";
    public static final String STOP_RECORDING = MwmActivity.class.getCanonicalName() + ".STOP_RECORDING";
    public static final String START_HELPER = MwmActivity.class.getCanonicalName() + ".START_HELPER";

    public static String[] PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
    };

    public static String[] PERMISSIONS_BACKGROUND = new String[]{ // should be called separately: https://developer.android.com/training/location/permissions#request-background-location
            Manifest.permission.ACCESS_BACKGROUND_LOCATION
    };

    Activity context;
    boolean start;
    ImageView recording;
    ImageView recordingStop;
    TextView recordingMenu;
    AppCompatThemeActivity.ScreenReceiver receiver;
    MapButtonsController buttons;

    public static void startActivity(Context context) {
        Intent i = new Intent(context, MwmActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(i);
    }

    public static void stopRecording(Context context, long time) {
        Intent intent = new Intent(STOP_RECORDING);
        intent.putExtra("time", time);
        context.sendBroadcast(intent);
    }

    public static void pauseRecording(Context context) {
        Intent intent = new Intent(PAUSE_RECORDING);
        context.sendBroadcast(intent);
    }

    public static void startHelper(Context context) {
        Intent i = new Intent(context, MwmActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.setAction(START_HELPER);
        context.startActivity(i);
    }

    public void create(Activity activity, MapButtonsController buttons) {
        RecordingService.init(activity);

        this.context = activity;
        this.buttons = buttons;

        receiver = new AppCompatThemeActivity.ScreenReceiver() {
            @Override
            public void onScreenOff() {
                if (!RecordingService.isRecording(context))
                    return;
                super.onScreenOff();
            }

            @Override
            public void onReceive(Context context, Intent intent) {
                super.onReceive(context, intent);
                String a = intent.getAction();
                if (a == null) return;
                if (a.equals(PAUSE_RECORDING)) {
                    if (TrackRecorder.INSTANCE.isEnabled()) {
                        TrackRecorder.INSTANCE.setEnabled(false);
                        RecordingService.updateService(context);
                    } else {
                        RecordingService.startRecording(context);
                    }
                    updateRecording();
                }
                if (a.equals(STOP_RECORDING)) {
                    TrackRecorder.INSTANCE.setEnabled(false);
                    long time = intent.getLongExtra("time", 0);
                    TrackRecorder.INSTANCE.nativeSaveTrack(RecordingService.SIMPLE_DATE_FORMAT.format(new Date(time)));
                    if (!start)
                        LocationHelper.INSTANCE.stop();
                    updateRecording();
                }
            }
        };
        receiver.filter.addAction(PAUSE_RECORDING);
        receiver.filter.addAction(STOP_RECORDING);
        receiver.registerReceiver(activity);

        String a = activity.getIntent().getAction();
        if (a != null && a.equals(START_HELPER)) { // by starting new activity we'll keep MainActivity in background
            activity.moveTaskToBack(true);
            activity.setIntent(new Intent());
        } else {
            RecordingService.startIfEnabled(context);
        }
        updateRecording();
    }

    public void updateRecording() {
        recording = (ImageView) buttons.findViewById(R.id.recording);
        recordingStop = (ImageView) buttons.findViewById(R.id.recording_stop);
        if (RecordingService.isRecording(context)) {
            if (TrackRecorder.INSTANCE.isEnabled())
                recording.setImageResource(R.drawable.ic_pause_black_48dp);
            else
                recording.setImageResource(R.drawable.ic_fiber_manual_record_red_48dp);
            recording.setColorFilter(ThemeUtils.getThemeColor(context, R.attr.iconTint));
            recordingStop.setVisibility(View.VISIBLE);
        } else {
            recording.setColorFilter(0xffff0000);
            recording.setImageResource(R.drawable.ic_fiber_manual_record_red_48dp);
            recordingStop.setVisibility(View.GONE);
        }
        recording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start();
            }
        });
        recordingStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stop();
            }
        });
    }

    public void onStart() { // activity
        start = true;
    }

    public void onStop() { // activity
        start = false;
        if (!RecordingService.isRecording(context))
            LocationHelper.INSTANCE.stop();
    }

    public void start() { // click
        if (RecordingService.isRecording(context)) { // pause/resume
            pauseRecording(context);
        } else { // start recording
            for (String s : PERMISSIONS) {
                if (ActivityCompat.checkSelfPermission(context, s) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(context, PERMISSIONS, 0);
                    return;
                }
            }
            if (Build.VERSION.SDK_INT >= 29) {
                for (String s : PERMISSIONS_BACKGROUND) {
                    if (ActivityCompat.checkSelfPermission(context, s) != PackageManager.PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(context, s))
                            ActivityCompat.requestPermissions(context, PERMISSIONS_BACKGROUND, 0);
                        return;
                    }
                }
            }
            RecordingService.startRecording(context);
            LocationHelper.INSTANCE.restart(); // update mInterval
            updateRecording();
        }
    }

    public void stop() { // click
        RecordingService.stopService(context);
        LocationHelper.INSTANCE.restart(); // update mInterval
    }

    public void destroy() {
        if (receiver != null) {
            receiver.close();
            receiver = null;
        }
    }
}
