package com.mapswithme.maps.location;

import static com.mapswithme.maps.MwmApplication.backgroundTracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mapswithme.util.CrashlyticsUtils;

public class TrackRecorderWakeReceiver extends BroadcastReceiver
{
  private static final String TAG = TrackRecorderWakeReceiver.class.getSimpleName();

  @Override
  public void onReceive(Context context, Intent intent)
  {
    String msg = "onReceive: " + intent + " app in background = "
                 + !backgroundTracker(context).isForeground();
    Log.i(TAG, msg);
    CrashlyticsUtils.INSTANCE.log(Log.INFO, TAG, msg);
    TrackRecorder.INSTANCE.onWakeAlarm();
  }
}
