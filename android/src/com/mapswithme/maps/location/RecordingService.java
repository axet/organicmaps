package com.mapswithme.maps.location;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.services.PersistentService;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.androidlibrary.widgets.RemoteNotificationCompat;
import com.mapswithme.maps.MwmApplication;
import com.mapswithme.maps.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class RecordingService extends PersistentService {
    public static final String TAG = RecordingService.class.getSimpleName();

    public static final int NOTIFICATION_RECORDING_ICON = 1;

    public static final String RECORDING = "recording";

    public static String PREF_OPTIMIZATION = "optimization";
    public static String PREF_NEXT = "next";

    public static String SHOW_ACTIVITY = RecordingService.class.getCanonicalName() + ".SHOW_ACTIVITY";
    public static String PAUSE_BUTTON = RecordingService.class.getCanonicalName() + ".PAUSE_BUTTON";
    public static String STOP_BUTTON = RecordingService.class.getCanonicalName() + ".STOP_BUTTON";
    public static String LOCATION_UPDATE = RecordingService.class.getCanonicalName() + ".LOCATION_UPDATE";
    public static String KEY_PROVIDER = "PROVIDER";

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");

    public static boolean INIT = false;

    public static void startRecording(Context context) {
        init(context);
        TrackRecorder.INSTANCE.setDuration(24);
        TrackRecorder.INSTANCE.setEnabled(true);
        RecordingService.startService(context);
    }

    public static void init(Context context) {
        MwmApplication app = (MwmApplication) context.getApplicationContext();
        if (!app.arePlatformAndCoreInitialized()) {
            try {
                app.init();
            } catch (java.io.IOException e) {
                throw new RuntimeException(e);
            }
        }
        if (!INIT) {
            TrackRecorder.INSTANCE.initialize(context);
            INIT = true;
        }
    }

    RecordingReceiver receiver;
    Handler handler;
    Runnable refresh = new Runnable() {
        @Override
        public void run() {
            refresh();
        }
    };
    LocationManager mLocationManager;
    ArrayList<PendingIntent> pe = new ArrayList<>();

    public class RecordingReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                // showRecordingActivity();
            }
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                // do nothing. do not annoy user. he will see alarm screen on next screen on event.
            }
        }
    }

    public static boolean isRecording(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        long time = shared.getLong(RECORDING, 0);
        return time != 0;
    }

    public static void startIfEnabled(Context context) {
        if (!isRecording(context)) return;
        startService(context);
        RecordingHelper.startHelper(context);
    }

    public static void startService(Context context) {
        if (!isRecording(context)) {
            SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor edit = shared.edit();
            edit.putLong(RECORDING, System.currentTimeMillis());
            edit.commit();
            TrackRecorder.INSTANCE.nativeClearTrack();
            updateBoot(context);
        }
        start(context, new Intent(context, RecordingService.class));
    }

    public static void updateService(Context context) {
        start(context, new Intent(context, RecordingService.class));
    }

    public static void stopService(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        long time = shared.getLong(RECORDING, 0);
        SharedPreferences.Editor edit = shared.edit();
        edit.putLong(RECORDING, 0);
        edit.commit();
        stop(context, new Intent(context, RecordingService.class));
        RecordingHelper.stopRecording(context, time);
        updateBoot(context);
    }

    public static void updateBoot(Context context) {
        PackageManager pm = context.getPackageManager();
        ComponentName name = new ComponentName(context.getPackageName(), OnBootReceiver.class.getCanonicalName());
        int e = isRecording(context) ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
        pm.setComponentEnabledSetting(name, e, 0);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        init(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
//        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, RecordingService.class.getCanonicalName() + "." + System.currentTimeMillis() + ".cpulock");
//        wakeLock.acquire();
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final List<String> providers = mLocationManager.getProviders(true);
        for (String s : Arrays.asList(LocationManager.NETWORK_PROVIDER, LocationManager.GPS_PROVIDER)) {
            if (providers.contains(s))
                continue;
            providers.add(s);
        }
        for (String provider : providers) {
            if (!mLocationManager.isProviderEnabled(provider)) {
                Log.d(TAG, "Provider disabled: " + provider);
                continue;
            }
            PendingIntent pe = PendingIntent.getService(this, 0, new Intent(this, RecordingService.class).setAction(LOCATION_UPDATE).putExtra(KEY_PROVIDER, provider), PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_MUTABLE);
            mLocationManager.requestLocationUpdates(provider, 1000, 0, pe);
            final Location location = mLocationManager.getLastKnownLocation(provider);
            if (location != null)
                LocationHelper.INSTANCE.onLocationChanged(location);
            this.pe.add(pe);
        }
    }

    @Override
    public void onCreateOptimization() {
        optimization = new OptimizationPreferenceCompat.ServiceReceiver(this, NOTIFICATION_RECORDING_ICON, PREF_OPTIMIZATION, PREF_NEXT) {
            @Override
            public boolean isOptimization() {
                return true;
            }

            @Override
            public void updateIcon() {
                icon.updateIcon(new Intent());
            }

            @Override
            public Notification build(Intent intent) {
                PendingIntent main = PendingIntent.getService(context, 0, new Intent(context, RecordingService.class).setAction(SHOW_ACTIVITY), PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                PendingIntent pe1 = PendingIntent.getService(context, 0, new Intent(context, RecordingService.class).setAction(PAUSE_BUTTON), PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                PendingIntent pe2 = PendingIntent.getService(context, 0, new Intent(context, RecordingService.class).setAction(STOP_BUTTON), PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
                long time = shared.getLong(RECORDING, 0);

                NotificationChannelCompat channel = new NotificationChannelCompat(context, "recording", "Recording", NotificationManagerCompat.IMPORTANCE_LOW);

                RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Builder(context, R.layout.notification);
                String title = getString(R.string.app_name);
                String text = SIMPLE_DATE_FORMAT.format(new Date(time)) + " - " + (TrackRecorder.INSTANCE.isEnabled() ? "recording" : "paused");
                builder.setImageViewResource(R.id.notification_pause, TrackRecorder.INSTANCE.isEnabled() ? R.drawable.ic_pause_black_48dp : R.drawable.ic_fiber_manual_record_red_48dp);
                builder.setOnClickPendingIntent(R.id.notification_pause, pe1);
                builder.setOnClickPendingIntent(R.id.notification_stop, pe2);

                builder.setTitle(title).setText(text).setMainIntent(main).setTheme(com.mapswithme.util.ThemeUtils.isNightTheme(context) ? R.style.MwmTheme_Night : R.style.MwmTheme).setChannel(channel).setWhen(icon.notification).setSmallIcon(R.drawable.ic_recording).setOngoing(true);

                if (!TrackRecorder.INSTANCE.isEnabled())
                    builder.setImageViewTint(R.id.notification_pause, Color.RED);

                return builder.build();
            }
        };
        optimization.create();
        init(this);
        handler = new Handler();
        receiver = new RecordingReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(receiver, filter);
        refresh();
    }

    @Override
    public void onStartCommand(Intent intent) {
        String a = intent.getAction();
        if (a == null) {
            optimization.updateIcon();
        } else if (a.equals(STOP_BUTTON)) {
            stopService(this);
        } else if (a.equals(PAUSE_BUTTON)) {
            RecordingHelper.pauseRecording(this);
        } else if (a.equals(SHOW_ACTIVITY)) {
            RecordingHelper.startActivity(this);
        } else if (a.equals(LOCATION_UPDATE)) {
            Log.d(TAG, "PendingIntentLocation: " + intent);
            Location[] ll = (Location[]) intent.getExtras().get(LocationManager.KEY_LOCATIONS);
            if (ll != null && ll.length > 0) {
                for (Location l : ll) {
                    if (l != null)
                        LocationHelper.INSTANCE.onLocationChanged(l);
                }
            } else {
                Location loc = (Location) intent.getExtras().get(android.location.LocationManager.KEY_LOCATION_CHANGED);
                if (loc != null)
                    LocationHelper.INSTANCE.onLocationChanged(loc);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestory");
        unregisterReceiver(receiver);
        handler.removeCallbacks(refresh);
        for (PendingIntent p : pe)
            mLocationManager.removeUpdates(p);
        pe.clear();
    }

    void refresh() {
        handler.removeCallbacks(refresh);
        optimization.updateIcon();
        handler.postDelayed(refresh, 10 * 1000);
    }
}
