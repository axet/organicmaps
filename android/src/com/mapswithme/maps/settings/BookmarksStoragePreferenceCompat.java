package com.mapswithme.maps.settings;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.preference.PreferenceManager;

import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.androidlibrary.preferences.StoragePathPreferenceCompat;
import com.mapswithme.maps.Framework;
import com.mapswithme.maps.R;
import com.mapswithme.maps.bookmarks.data.BookmarkManager;
import com.mapswithme.util.StorageUtils;

import java.io.File;
import java.util.ArrayList;

public class BookmarksStoragePreferenceCompat extends StoragePathPreferenceCompat {
    public static final String TAG = BookmarksStoragePreferenceCompat.class.getSimpleName();

    public static String[] all(String[] a1, String[] a2) {
        String[] both = new String[a1.length + a2.length];
        System.arraycopy(a1, 0, both, 0, a1.length);
        System.arraycopy(a2, 0, both, a1.length, a2.length);
        return both;
    }

    public static Uri getBookmarksPath(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        String path = shared.getString("bookmarks_path", StorageUtils.getSettingsPath((Application) context.getApplicationContext()));
        Storage storage = new Storage(context);
        return storage.getStoragePath(path);
    }

    public static String endSlash(String s) {
        if (s.endsWith("/"))
            return s;
        return s + "/";
    }

    public static Uri parse(String uri) {
        Uri u;
        if (uri.startsWith(ContentResolver.SCHEME_CONTENT)) {
            u = Uri.parse(uri);
        } else if (uri.startsWith(ContentResolver.SCHEME_FILE)) {
            u = Uri.parse(uri);
        } else {
            u = Uri.parse(ContentResolver.SCHEME_FILE + Storage.CSS + uri);
        }
        return u;
    }

    public BookmarksStoragePreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public BookmarksStoragePreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public BookmarksStoragePreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BookmarksStoragePreferenceCompat(Context context) {
        super(context);
    }

    public void create() {
        super.create();
        setValue(getBookmarksPath(getContext()).toString());
    }

    @Override
    public boolean callChangeListener(Object newValue) {
        update(newValue);
        return false; // super.callChangeListener(newValue);
    }

    public void setValue(String s) {
        setText(s);
        updatePath(s);
    }

    public void setText(String s) {
        super.setText(s);
        Log.d(TAG, "setText " + s);
    }

    public void saveValue(String s) {
        Log.d(TAG, "saveValue " + s);
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor edit = shared.edit();
        edit.putString(getKey(), s);
        edit.commit();
        setValue(s);
        Framework.nativeSetBookmarksDir(s);
        BookmarkManager.loadBookmarks();
    }

    public void update(Object newValue) {
        Log.d(TAG, "update " + (String) newValue);
        final Uri pathOld = storage.getStoragePath(getText());
        final Uri pathNew = parse((String) newValue);
        if (pathOld.equals(pathNew)) {
            saveValue(pathNew.toString()); // updatePath, permissions, and sync bookmarks
            return;
        }
        String s = pathOld.getScheme();
        if (!s.equals(ContentResolver.SCHEME_FILE)) { // only migrate for old File storage
            saveValue(pathNew.toString());
            return;
        }
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setCancelable(false);
        b.setTitle(R.string.bookmarks_move);
        b.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dlg, int which) {
                try {
                    Log.d(TAG, "List old storage: " + pathOld);
                    ArrayList<Storage.Node> nn = Storage.list(getContext(), pathOld, new Storage.NodeFilter() {
                        @Override
                        public boolean accept(Storage.Node file) {
                            String s = file.name.toLowerCase();
                            for (String e : all(Framework.nativeGetBookmarksFilesExts(), new String[]{".kmb", ".kml"})) {
                                if (s.endsWith(e.toLowerCase()))
                                    return true;
                            }
                            return false;
                        }
                    });
                    for (Storage.Node n : nn) {
                        File f = Storage.getFile(n.uri);
                        Storage.migrate(getContext(), f, pathNew);
                    }
                    saveValue(pathNew.toString());
                } catch (RuntimeException e) {
                    Log.w(TAG, e);
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.bookmarks_move)
                            .setMessage(e.getMessage())
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .show();
                }
            }
        });
        b.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dlg, int which) {
            }
        });
        b.show();
    }
}
