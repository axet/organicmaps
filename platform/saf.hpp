#pragma once

#include <functional>
#include <string>

#include "coding/file_reader.hpp"
#include "coding/file_writer.hpp"
#include "platform/platform.hpp"

struct _JNIEnv;
class _jobject;
typedef _JNIEnv JNIEnv;
typedef _jobject * jobject;

namespace platform
{
  extern std::string FilesName(int i);
  extern int FilesCount(const std::string &path);
  extern int FilesType(const std::string & path); // 0 - dir, 1 - file
  extern bool FilesExists(const std::string & path);
  extern uint64_t FilesSize(const std::string &path);
  extern int FilesWrite(const std::string &path, uint64_t off, void const *buf, int pos, int size);
  extern int FilesRead(const std::string &path, uint64_t off, void const *buf, int pos, int size);
  extern bool FilesDelete(const std::string &path);
  extern bool FilesMkdir(const std::string &path);
  extern bool FilesMove(const std::string &from, const std::string &to);

  extern void FilesByExt(std::string const & dir, std::string const & ext, Platform::FilesList& files); // Platform::GetFilesByExt

  extern bool WriteToTempAndRenameToFile(std::string const & dest, std::function<bool(std::string const &)> const & write);

  class FilesPlatformWriter: public Writer { // coding/writer.hpp, coding/file_writer.hpp
  protected:
    std::string path;
    uint64_t pos;
  public:
    FilesPlatformWriter(const std::string &path) { this->path = path; this->pos = 0; }
    virtual ~FilesPlatformWriter() {};
    virtual void Seek(uint64_t pos) { this->pos = pos; }
    virtual uint64_t Pos() const { return pos; }
    virtual void Write(void const * p, size_t size) { pos += FilesWrite(path, pos, p, 0, size); }
  };

  class FilesWriter : public FilesPlatformWriter {
    char buf[4096];
    uint64_t pos; // file position
    uint64_t bufstart; // [buffer start position
    uint64_t bufend; // buffer end position)
  public:
    FilesWriter(const std::string &path) : FilesPlatformWriter(path) { pos = 0; bufstart = 0; bufend = 0; }
    virtual ~FilesWriter() {
      Flush();
    }
    virtual void Seek(uint64_t pos) {
      this->pos = pos;
    }
    virtual uint64_t Pos() const {
      return pos;
    }
    virtual void Write(void const * p, size_t size) {
      if (pos < bufstart || pos > bufend) // off buffer
        Flush();
      uint64_t bufmax = bufstart + sizeof(buf); // max)
      uint64_t posend = pos + size; // end)
      if (pos >= bufstart && pos <= bufend && posend <= bufmax) { // fits
        char *dst = buf + (pos - bufstart);
        memcpy(dst, p, size);
        this->pos += size;
        bufend = this->pos;
      } else if (pos >= bufstart && pos <= bufend && posend > bufmax) { // partial
        char *dst = buf + (pos - bufstart);
        int par = bufmax - pos; // size of 'left' part of the write buffer
        if (par > 0) { // write 'left' part into buffer
          memcpy(dst, p, par);
          this->pos += par;
          bufend = this->pos; // bufend == bufstart + sizeof(buf) == this->pos
          p = ((char*)p) + par;
          size -= par;
        }
        Flush();
        if ( size <= sizeof(buf)) // rest of the write buffer is small (buffer is big)
          Write(p, size); // recursion
        else // rest of the write buffer is bigger then cache (buffer too small)
          this->pos += FilesWrite(path, this->pos, p, 0, size);
      }
    }
    void Flush() {
      int len = bufend - bufstart;
      if ( len > 0 )
        FilesWrite(path, bufstart, buf, 0, len);
      bufstart = pos;
      bufend = pos;
    }
  };

  class FilesPlatformReader: public Reader { // coding/reader.hpp, coding/file_reader.hpp
  protected:
    std::string path;
    uint64_t pos;
    uint64_t size;
  public:
    FilesPlatformReader(const std::string &path) { this->path = path; pos = 0; size = FilesSize(this->path); };
    FilesPlatformReader(const std::string &path, int pos, int size) { this->path = path; this->pos = pos; this->size = size; };
    virtual ~FilesPlatformReader() {}
    virtual uint64_t Size() const { return size; }
    virtual void Read(uint64_t pos, void * p, size_t size) const { FilesRead(path, this->pos + pos, p, 0, size); }
    virtual std::unique_ptr<Reader> CreateSubReader(uint64_t pos, uint64_t size) const { return std::make_unique<FilesPlatformReader>(this->path, pos, static_cast<size_t>(size)); }
  };

  class FilesReader: public FilesPlatformReader { // TODO implement buffered file reader
  public:
    FilesReader(const std::string &path):FilesPlatformReader(path) {};
    FilesReader(const std::string &path, int pos, int size):FilesPlatformReader(path, pos, size) {};
    virtual ~FilesReader() {}
    virtual uint64_t Size() const { return FilesPlatformReader::Size(); }
    virtual void Read(uint64_t pos, void * p, size_t size) const { FilesPlatformReader::Read(pos, p, size); }
    virtual std::unique_ptr<Reader> CreateSubReader(uint64_t pos, uint64_t size) const { return std::make_unique<FilesReader>(this->path, pos, static_cast<size_t>(size)); }
  };
}  // namespace platform

